import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {ForbiddenComponent} from './pages/forbidden/forbidden.component';


const appRoutes: Routes = [
  {path: 'forbidden', component: ForbiddenComponent},
  {path: 'not-found', component: NotFoundComponent}
];

@NgModule({
  declarations: [NotFoundComponent, ForbiddenComponent],
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
