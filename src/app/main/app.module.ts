import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {EmployeesModule} from '../employees/employees.module';
import {ProcessingModule} from '../business-processing/processing.module';


import {AppComponent} from './components/app-component/app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {LogService} from '../core/services/log.service';
import {CoreModule} from '../core/core.module';
import {AuthenticationModule} from '../authentication/authentication.module';
import {AuthService} from '../authentication/auth.service';
import {SettingsService} from '../core/services/settings.service';
import {DatetimeService} from '../core/services/datetime.service';
import {EmployeeService} from '../employees/services/employee.service';
import {DictionariesService} from './services/dictionaries.service';
import {ForeignService} from '../employees/services/foreign.service';
import {Guid} from '../core/services/guid.service';
import {ClientsModule} from '../clients/clients.module';
import {FinancesModule} from '../finances/finances.module';

export const MY_NATIVE_FORMATS = {
  fullPickerInput: {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric'},
  datePickerInput: {month: 'short', day: 'numeric', year: 'numeric'},
  timePickerInput: {hour: 'numeric', minute: 'short'},
  monthYearLabel: {year: 'numeric', month: 'short'},
  dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
  monthYearA11yLabel: {year: 'numeric', month: 'long'},
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    EmployeesModule,
    FinancesModule,
    ClientsModule,
    ProcessingModule,
    CoreModule,
    AuthenticationModule
  ],
  entryComponents: [],
  providers: [
    LogService,
    AuthService,
    SettingsService,
    DatetimeService,
    EmployeeService,
    ForeignService,
    DictionariesService,
    Guid],
  bootstrap: [AppComponent]
})
export class AppModule {
}
