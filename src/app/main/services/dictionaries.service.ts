import {Injectable} from '@angular/core';
import {LogService} from '../../core/services/log.service';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class DictionariesService {
  constructor(private logger: LogService, private http: HttpClient) {
  }

  public GetAccounts(): any {
    return this.http
      .get('http://localhost:20822/Accounts/All')
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }
}
