import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreModule} from '../core/core.module';
import { ClientsInvoicingComponent } from './components/pages/clients-invoicing/clients-invoicing.component';
import {FinancesRoutingModule} from './finances-routing.module';
import { ClientsPaymentComponent } from './components/pages/clients-payment/clients-payment.component';

@NgModule({
  declarations: [ClientsInvoicingComponent, ClientsPaymentComponent],
  imports: [
    FinancesRoutingModule,
    CommonModule,
    CoreModule],
  exports: [],
  entryComponents: [],
  providers: [],
})
export class FinancesModule {
}
