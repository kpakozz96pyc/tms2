import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientsInvoicingComponent} from './components/pages/clients-invoicing/clients-invoicing.component';
import {ClientsPaymentComponent} from './components/pages/clients-payment/clients-payment.component';

const clientRoutes: Routes = [
  {path: 'clientsInvoicing', component: ClientsInvoicingComponent},
  {path: 'clientsPayment', component: ClientsPaymentComponent}
];

@NgModule({
  imports: [RouterModule.forChild(clientRoutes)],
  exports: [RouterModule],
})
export class FinancesRoutingModule {
}
