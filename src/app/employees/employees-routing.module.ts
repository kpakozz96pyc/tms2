import {NgModule} from '@angular/core';
import {EmployeesComponent} from './inhouses/components/employees/employees.component';
import {MyDashboardComponent} from './inhouses/components/my-dashboard/my-dashboard.component';
import {RouterModule, Routes} from '@angular/router';


const employeesRoutes: Routes = [
  {path: 'home', component: MyDashboardComponent},
  {path: 'employees', component: EmployeesComponent},
];

@NgModule({
  imports: [RouterModule.forChild(employeesRoutes)],
  exports: [RouterModule],
})
export class EmployeesRoutingModule {
}
