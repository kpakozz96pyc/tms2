import {Component, Injectable, OnInit} from '@angular/core';
import {AuthService} from '../../../../authentication/auth.service';

@Component({
  selector: 'px-my-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: ['./my-dashboard.component.scss']
})
@Injectable()
export class MyDashboardComponent implements OnInit {

  constructor(private auth: AuthService) {
  }

  ngOnInit() {
  }

  changeAuthStatus(status: string) {
    if (status === 'login') {
      this.auth.logIn();
    } else {
      this.auth.logOut();
    }

  }

}
