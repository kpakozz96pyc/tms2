import {LogService} from '../../core/services/log.service';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ForeignService {
  constructor(private logger: LogService, private http: HttpClient) {
  }

  public GetList(organizationId: string): any {
    return this.http
      .get('http://localhost:20822/ForeignersList/ByOrganizationId', {params: {organizationId: organizationId}})
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }
}
