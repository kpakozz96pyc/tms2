import { LogService } from '../../core/services/log.service';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
  constructor(private logger: LogService, private http: HttpClient) {
  }

  public GetCurrentUser(): any {
    return this.http
      .get('http://localhost:20822/Users/current')
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }

  public GetPossibleProjectCustomers(): any {
    return this.http
      .get('http://localhost:20822/StakeholdersList/PossibleProjectCustomers')
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }

  public GetPossibleProjectAssignees(): any {
    return this.http
      .get('http://localhost:20822/StakeholdersList/PossibleProjectAssignees')
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }
}
