import {NgModule} from '@angular/core';
import {EmployeesComponent} from './inhouses/components/employees/employees.component';
import {MyDashboardComponent} from './inhouses/components/my-dashboard/my-dashboard.component';
import {CommonModule} from '@angular/common';
import {EmployeesRoutingModule} from './employees-routing.module';

@NgModule({
  declarations: [
    EmployeesComponent,
    MyDashboardComponent],
  imports: [
    CommonModule,
    EmployeesRoutingModule
  ],
  exports: [],
  providers: [],
})
export class EmployeesModule {
}
