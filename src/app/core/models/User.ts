
export class User  {
  id: string;
  name: string;
  photo: string;
  description: string;
}
