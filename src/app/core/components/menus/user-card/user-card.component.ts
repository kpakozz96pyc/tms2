import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/User';
import {EmployeeService} from '../../../../employees/services/employee.service';

@Component({
  selector: 'px-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
  User: User;

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.User = this.employeeService.GetCurrentUser();
  }

}
