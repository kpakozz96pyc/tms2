import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';

import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators';
import {map} from 'rxjs/operators';

@Component({
  selector: 'px-search-select',
  templateUrl: './search-select.component.html',
  styleUrls: ['./search-select.component.scss']
})
export class SearchSelectComponent {
  @Input()
  list: any[];

  stateCtrl: FormControl;
  filteredList: Observable<any[]>;

  constructor() {
    this.stateCtrl = new FormControl();
    this.filteredList = this.stateCtrl.valueChanges
      .pipe(
        startWith(''),
        map(item => item ? this.filterStates(item as string) : this.list.slice())
      );
  }

  filterStates(name: string) {
    return this.list.filter(item =>
      item.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

}


