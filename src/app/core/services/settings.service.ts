import {Injectable} from '@angular/core';

@Injectable()
export class SettingsService {
  fractionSize = 2;
  modelDebounce = 600;
  gridItemsPerPage = 25;
  dateFormat = 'dd MMM yyyy';
  dateTimeFormat = 'dd MMM yyyy. HH:mm';
  languageCode = 'en-US';
}
