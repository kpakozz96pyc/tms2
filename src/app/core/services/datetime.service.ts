import * as moment from 'moment';
import {Injectable} from '@angular/core';

@Injectable()
export class DatetimeService {
  getTodayStart = () => {
    return moment().startOf('d').unix();
  }

  getTodayEnd = () => {
    return moment().startOf('d').add(1, 'd').add(-1, 's').unix();
  }
}
