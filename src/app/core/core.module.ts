import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LogoComponent} from './components/menus/logo/logo.component';
import {FooterComponent} from './components/menus/footer/footer.component';
import {UserCardComponent} from './components/menus/user-card/user-card.component';
import {LeftMenuComponent} from './components/menus/left-menu/left-menu.component';
import {NavigationMenuComponent} from './components/menus/navigation-menu/navigation-menu.component';
import {RouterModule} from '@angular/router';
import {PxDate} from './pipes/pxDate.pipe';
import {PxDateTime} from './pipes/pxDateTime.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCheckboxModule,
  MatDatepickerModule, MatDialogModule, MatInputModule, MatNativeDateModule,
  MatSelectModule,
  MatTooltipModule
} from '@angular/material';
import {MatPaginatorModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EnumPipe} from './pipes/enum.pipe';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {SearchSelectComponent} from './components/ui-elements/search-select/search-select.component';
import {DropDownHeaderMenuComponent} from './components/ui-elements/drop-down-header-menu/drop-down-header-menu.component';
import {DropDownCornerMenuComponent} from './components/ui-elements/drop-down-corner-menu/drop-down-corner-menu.component';


@NgModule({
  declarations: [
    LogoComponent,
    FooterComponent,
    UserCardComponent,
    NavigationMenuComponent,
    LeftMenuComponent,
    PxDate,
    PxDateTime,
    EnumPipe,
    SearchSelectComponent,
    DropDownCornerMenuComponent,
    DropDownHeaderMenuComponent],
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTooltipModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule
  ],
  exports: [
    LogoComponent,
    FooterComponent,
    UserCardComponent,
    NavigationMenuComponent,
    LeftMenuComponent,
    PxDate,
    PxDateTime,
    EnumPipe,
    DropDownHeaderMenuComponent,
    DropDownCornerMenuComponent,
    BrowserAnimationsModule,
    MatInputModule,
    MatPaginatorModule,
    MatButtonModule,
    FormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    SearchSelectComponent,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule
  ]
})
export class CoreModule {
}
