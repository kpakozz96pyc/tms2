import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectsListComponent} from './projects/components/pages/projects-list/projects-list.component';
import {ProjectDashboardComponent} from './projects/components/pages/project-dashboard/project-dashboard.component';
import {GoalsListComponent} from './goals/components/pages/goals-list/goals-list.component';

const processingRoutes: Routes = [
  {path: 'projects', component: ProjectsListComponent},
  {path: 'project/:id', component: ProjectDashboardComponent},
  {path: 'goals', component: GoalsListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(processingRoutes)],
  exports: [RouterModule]
})
export class ProcessingRoutingModule {
}
