import {Component, Injectable, OnInit} from '@angular/core';
import {ProjectService} from '../../../../services/project.service';
import {MatDialog, PageEvent} from '@angular/material';
import {SettingsService} from '../../../../../core/services/settings.service';
import {DatetimeService} from '../../../../../core/services/datetime.service';
import {ProjectStatusEnum} from '../../../../enums/project-status.enum';
import {CreateProjectWithGoalComponent} from '../../ui-elements/create-project-with-goal/create-project-with-goal.component';


@Component({
  selector: 'px-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
@Injectable()
export class ProjectsListComponent implements OnInit {

  Projects = [];
  Filter;
  listLength = 100;
  ProjectStatuses;

  constructor(private  projectService: ProjectService,
              private settings: SettingsService,
              private datetime: DatetimeService,
              public dialog: MatDialog) {
  }

  getServerData($event: PageEvent) {
    this.Filter.itemsPerPage = $event.pageSize;
    this.Filter.currentPage = $event.pageIndex + 1;
    this.Search();
  }

  Search = () => {
    this.Projects = [];
    this.loadProjects(this.Filter);
  }

  loadProjects = (Filter) => {
    return this.projectService.GetProjectsList(Filter)
      .subscribe(x => {
        this.Projects = x.items;
        this.listLength = x.totalItems;
      });
  }

  resetFilterClick = () => {
    this.resetFilter();
    this.Search();
  }

  createProject() {
    this.dialog.open(CreateProjectWithGoalComponent, {
      width: '500px',
    });
  }

  resetFilter = () => {
    this.Filter = {
      itemsPerPage: this.settings.gridItemsPerPage,
      currentPage: 1,
      todayStart: this.datetime.getTodayStart(),
      todayEnd: this.datetime.getTodayEnd()
    };
  }

  ngOnInit() {
    this.ProjectStatuses = ProjectStatusEnum;
    this.Projects = [];
    this.resetFilter();
    this.loadProjects(this.Filter);
  }

}
