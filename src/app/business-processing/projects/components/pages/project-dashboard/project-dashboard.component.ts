import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProjectService} from '../../../../services/project.service';

@Component({
  selector: 'px-project-dashboard',
  templateUrl: './project-dashboard.component.html',
  styleUrls: ['./project-dashboard.component.scss']
})
export class ProjectDashboardComponent implements OnInit {
  id: string;
  ProjectDashboard;

  constructor(private route: ActivatedRoute, private projectService: ProjectService) {

  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.projectService.GetProjectDashboard(this.id).subscribe((data) => {
      this.ProjectDashboard = data;
    });
  }

}
