import {Component, Input, OnInit} from '@angular/core';
import {ProjectStatusEnum} from '../../../../enums/project-status.enum';

@Component({
  selector: 'px-project-status-marker',
  templateUrl: './project-status-marker.component.html',
  styleUrls: ['./project-status-marker.component.scss']
})
export class ProjectStatusMarkerComponent implements OnInit {

  @Input()
  Code: ProjectStatusEnum;
  markClass: string;
  mark: string;
  name: string;

  constructor() {
  }

  private _setClass = () => {
    switch (this.Code) {
      case ProjectStatusEnum.Planning: {
        this.markClass = 'mark-planning';
        break;
      }
      case ProjectStatusEnum.Production: {
        this.markClass = 'mark-production';
        break;
      }
      case ProjectStatusEnum.OnHold: {
        this.markClass = 'mark-hold';
        break;
      }
      case ProjectStatusEnum.Closed: {
        this.markClass = 'mark-closed';
        break;
      }
      case ProjectStatusEnum.Cancelled: {
        this.markClass = 'mark-cancelled';
        break;
      }
      default: {
        this.markClass = '';
        break;
      }
    }
  }
  private _setText = () => {
    this.name = ProjectStatusEnum[this.Code];

    if (this.Code === ProjectStatusEnum.OnHold) {
      this.mark = 'H';
    } else {
      this.mark = this.name[0];
    }
  }

  ngOnInit() {
    this._setClass();
    this._setText();
  }

}
