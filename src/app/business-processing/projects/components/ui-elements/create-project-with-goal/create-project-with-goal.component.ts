import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {GoalService} from '../../../../services/goal.service';
import {EmployeeService} from '../../../../../employees/services/employee.service';
import {DictionariesService} from '../../../../../main/services/dictionaries.service';
import {ForeignService} from '../../../../../employees/services/foreign.service';
import {ProjectService} from '../../../../services/project.service';
import {CreateProjectCommand} from '../../../../models/commands/createProject.command';
import {Guid} from '../../../../../core/services/guid.service';

@Component({
  selector: 'px-create-project-with-goal',
  templateUrl: './create-project-with-goal.component.html',
  styleUrls: ['./create-project-with-goal.component.scss']
})
export class CreateProjectWithGoalComponent implements OnInit {

  Customers;
  Assignees;
  Accounts;
  Goals;
  ClientManagers;
  // Todo: replace with type
  Project: any = {customer: {}};


  private _loadClients() {
    this.Customers = this.employeeService.GetPossibleProjectCustomers();
  }

  private _loadAssignees() {
    this.Assignees = this.employeeService.GetPossibleProjectAssignees();
  }

  private _loadAccounts() {
    this.Accounts = this.dictionariesService.GetAccounts();
  }

  private _loadGoals() {
    this.Goals = this.goalService.GetAll();
  }

  public loadClientManagers() {
    if (this.Project.customer.id) {
      this.Project.clientManager = undefined;
      this.ClientManagers = this.foreignService.GetList(this.Project.customer.id);
    }
  }

  public createProject() {
    const command = new CreateProjectCommand();
    command.projectId = Guid.New();
    command.accountId = this.Project.account.id;
    command.assigneeId = this.Project.assignee.id;
    command.customerId = this.Project.customer.id;
    command.clientManagerId = this.Project.clientManager && this.Project.clientManager.id || null;
    command.goalId = this.Project.goal.id;
    command.deadline = this.Project.deadline.getTime() / 1000;
    command.name = this.Project.name;
    this.projectService.CreateProjectWithGoal(command).subscribe(() => {
      this.dialog.closeAll();
    });
  }

  constructor(public dialog: MatDialog,
              private goalService: GoalService,
              private employeeService: EmployeeService,
              private foreignService: ForeignService,
              private dictionariesService: DictionariesService,
              private projectService: ProjectService) {
  }

  ngOnInit() {
    this._loadGoals();
    this._loadClients();
    this._loadAssignees();
    this._loadAccounts();
  }
}
