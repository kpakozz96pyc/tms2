import { Injectable } from '@angular/core';
import { LogService } from '../../core/services/log.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { CreateProjectCommand } from '../models/commands/createProject.command';

@Injectable()
export class ProjectService {
  constructor(private logger: LogService, private http: HttpClient) {
  }

  // Queries
  public GetProjectsList(Filter): any {
    return this.http
      .get('http://localhost:20822/ProjectsList/List', { params: Filter })
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }

  public GetProjectDashboard(id: string) {
    return this.http
      .get('http://localhost:20822/ProjectDashboards/Details', { params: { id: id } })
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }

  // Commands
  public CreateProjectWithGoal(command: CreateProjectCommand) {
    return this.http
      .post('http://localhost:20822/CreateProject', command)
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }
}
