import { Injectable } from '@angular/core';
import { LogService } from '../../core/services/log.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { CreateProjectCommand } from '../models/commands/createProject.command';
import { CreateGoalCommand } from '../models/commands/createGoal.command';

@Injectable()
export class GoalService {
  constructor(private logger: LogService, private http: HttpClient) {
  }

  // Queries
  public GetAll(): any {
    return this.http
      .get('http://localhost:20822/GoalList/All')
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }

  // Commands
  public CreateGoal(command: CreateGoalCommand) {
    return this.http
      .post('http://localhost:20822/CreateGoal', command)
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }
}
