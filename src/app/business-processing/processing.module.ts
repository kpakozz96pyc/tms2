import {NgModule} from '@angular/core';
import {ProjectsListComponent} from './projects/components/pages/projects-list/projects-list.component';
import {ProjectDashboardComponent} from './projects/components/pages/project-dashboard/project-dashboard.component';
import {ProjectBudgetComponent} from './projects/components/pages/project-budget/project-budget.component';
import {CommonModule} from '@angular/common';
import {ProjectService} from './services/project.service';
import {ProcessingRoutingModule} from './processing-routing.module';
import {CoreModule} from '../core/core.module';
import {ProjectStatusMarkerComponent} from './projects/components/ui-elements/project-status-marker/project-status-marker.component';
import {CreateProjectWithGoalComponent} from './projects/components/ui-elements/create-project-with-goal/create-project-with-goal.component';
import {GoalService} from './services/goal.service';
import {EmployeesModule} from '../employees/employees.module';
import { GoalsListComponent } from './goals/components/pages/goals-list/goals-list.component';
import { CreateGoalComponent } from './goals/components/ui-components/create-goal/create-goal.component';

@NgModule({
  declarations: [
    ProjectsListComponent,
    ProjectDashboardComponent,
    ProjectBudgetComponent,
    ProjectStatusMarkerComponent,
    CreateProjectWithGoalComponent,
    GoalsListComponent,
    CreateGoalComponent],
  imports: [
    CommonModule,
    CoreModule,
    ProcessingRoutingModule,
    EmployeesModule
  ],
  entryComponents: [
    CreateProjectWithGoalComponent,
    CreateGoalComponent],
  providers: [ProjectService, GoalService],
})
export class ProcessingModule {
}
