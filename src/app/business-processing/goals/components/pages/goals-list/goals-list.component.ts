import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {GoalService} from '../../../../services/goal.service';
import {CreateGoalComponent} from '../../ui-components/create-goal/create-goal.component';

@Component({
  selector: 'px-goals-list',
  templateUrl: './goals-list.component.html',
  styleUrls: ['./goals-list.component.scss']
})
export class GoalsListComponent implements OnInit {

  Goals;

  createGoal() {
    this.dialog.open(CreateGoalComponent, {
      width: '500px',
    });
  }

  constructor(private  goalService: GoalService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.Goals = this.goalService.GetAll();
  }

}
