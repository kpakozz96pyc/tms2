import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import {EmployeeService} from '../../../../../employees/services/employee.service';
import {CreateProjectCommand} from '../../../../models/commands/createProject.command';
import {Guid} from '../../../../../core/services/guid.service';
import {CreateGoalCommand} from '../../../../models/commands/createGoal.command';
import {GoalService} from '../../../../services/goal.service';

@Component({
  selector: 'px-create-goal',
  templateUrl: './create-goal.component.html',
  styleUrls: ['./create-goal.component.scss']
})
export class CreateGoalComponent implements OnInit {
  Goal: any = {};
  InHouses;

  public createGoal() {
    const command = new CreateGoalCommand();
    command.id = Guid.New();
    command.assigneeId = this.Goal.assignee.id;
    command.customerId = this.Goal.customer.id;
    command.name = this.Goal.name;
    this.goalService.CreateGoal(command).subscribe(() => {
      this.dialog.closeAll();
    });
  }

  constructor(public dialog: MatDialog,
              private employeeService: EmployeeService,
              private goalService: GoalService) { }

  ngOnInit() {
    this.InHouses = this.employeeService.GetPossibleProjectAssignees();
  }

}
