export enum ProjectStatusEnum {
  Active = 5, // Planning + Production
  Planning = 0,
  Production = 1,
  Today = 11, // Production with today deadline
  Weekly = 13, // Production with weekly deadline
  Overdue = 12, // Production with overdue deadline
  OnHold = 2,
  Closed = 3,
  Cancelled = 4
}
