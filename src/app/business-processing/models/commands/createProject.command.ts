export class CreateProjectCommand {
  projectId: string;
  name: string;
  goalId: string;
  customerId: string;
  assigneeId: string;
  accountId: string;
  clientManagerId: string;
  deadline: number;
}
