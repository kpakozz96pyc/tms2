export class CreateGoalCommand {
  id: string;
  name: string;
  customerId: string;
  assigneeId: string;
  parentId: string;
}
