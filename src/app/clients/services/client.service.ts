import {Injectable} from '@angular/core';
import {LogService} from '../../core/services/log.service';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import {RegisterClientCommand} from '../models/registerClient.command';

@Injectable()
export class ClientService {
  constructor(private logger: LogService, private http: HttpClient) {
  }

  // Queries
  public GetClients(Filter): any {
    return this.http
      .get('http://localhost:20822/ClientsList/List', {params: Filter})
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }

  public GetClient(id: string): any {
    return this.http
      .get('http://localhost:20822/ClientDashboards/Details', {params: {id: id}})
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }
  // Commands
  public RegisterClient(command: RegisterClientCommand) {
    return this.http
      .post('http://localhost:20822/RegisterClient', command)
      .pipe(
        map((response: Response) => {
          return response;
        }),
        catchError((error: Response) => {
          return Observable.throw('error get request');
        }));
  }

}
