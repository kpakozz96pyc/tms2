export class RegisterClientCommand {
  id: string;
  name: string;
  fullName: string;
  isLsp: boolean;
}
