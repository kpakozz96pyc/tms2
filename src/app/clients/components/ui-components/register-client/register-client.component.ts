import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ClientService} from '../../../services/client.service';
import {Guid} from '../../../../core/services/guid.service';

@Component({
  selector: 'px-register-client',
  templateUrl: './register-client.component.html',
  styleUrls: ['./register-client.component.scss']
})
export class RegisterClientComponent implements OnInit {

  // Todo: replace with type
  Client: any = {};

  public registerClient() {
    const command = this.Client;
    command.id = Guid.New();
    this.clientSrvice.RegisterClient(command).subscribe(() => {
      this.dialog.closeAll();
    });
  }

  constructor(public dialog: MatDialog,
              private clientSrvice: ClientService) {
  }

  ngOnInit() {

  }
}
