import { Component, OnInit } from '@angular/core';
import {MatDialog, PageEvent} from '@angular/material';
import {SettingsService} from '../../../../core/services/settings.service';
import {ClientService} from '../../../services/client.service';
import {CreateProjectWithGoalComponent} from '../../../../business-processing/projects/components/ui-elements/create-project-with-goal/create-project-with-goal.component';
import {RegisterClientComponent} from '../../ui-components/register-client/register-client.component';

@Component({
  selector: 'px-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {

  Clients = [];
  Filter;
  listLength = 100;

  resetFilter = () => {
    this.Filter = {
      itemsPerPage: this.settings.gridItemsPerPage,
      currentPage: 1,
    };
  }

  resetFilterClick = () => {
    this.resetFilter();
    this.Search();
  }

  private loadClients = (Filter) => {
    return this.clientService.GetClients(Filter)
      .subscribe(x => {
        this.Clients = x.items;
        this.listLength = x.totalItems;
      });
  }

  getServerData($event: PageEvent) {
    this.Filter.itemsPerPage = $event.pageSize;
    this.Filter.currentPage = $event.pageIndex + 1;
    this.Search();
  }

  Search = () => {
    this.Clients = [];
    this.loadClients(this.Filter);
  }

  registerClient() {
    const dialogRef = this.dialog.open(RegisterClientComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  constructor(private settings: SettingsService,
              private clientService: ClientService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.Clients = [];
    this.resetFilter();
    this.loadClients(this.Filter);
  }

}
