import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../../services/client.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'px-client-dashboard',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.scss']
})
export class ClientDashboardComponent implements OnInit {

  id: string;
  ClientDashboard;

  constructor(private route: ActivatedRoute, private clientService: ClientService) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.clientService.GetClient(this.id).subscribe((data) => {
      this.ClientDashboard = data;
    });
  }

}
