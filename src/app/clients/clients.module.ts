import {NgModule} from '@angular/core';
import {ClientsListComponent} from './components/pages/clients-list/clients-list.component';
import {ClientsRoutingModule} from './clients-routing.module';
import {CommonModule} from '@angular/common';
import {CoreModule} from '../core/core.module';
import {ClientService} from './services/client.service';
import {ClientMarkerComponent} from './components/ui-components/client-marker/client-marker.component';
import {RegisterClientComponent} from './components/ui-components/register-client/register-client.component';
import {ClientDashboardComponent} from './components/pages/client-dashboard/client-dashboard.component';

@NgModule({
  declarations: [ClientsListComponent, ClientMarkerComponent, RegisterClientComponent, ClientDashboardComponent],
  imports: [
    ClientsRoutingModule,
    CommonModule,
    CoreModule],
  exports: [],
  entryComponents: [RegisterClientComponent],
  providers: [ClientService],
})
export class ClientsModule {
}
