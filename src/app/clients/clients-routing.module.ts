import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientsListComponent} from './components/pages/clients-list/clients-list.component';
import {ClientDashboardComponent} from './components/pages/client-dashboard/client-dashboard.component';


const clientRoutes: Routes = [
  {path: 'clients', component: ClientsListComponent},
  {path: 'client/:id', component: ClientDashboardComponent},
];

@NgModule({
  imports: [RouterModule.forChild(clientRoutes)],
  exports: [RouterModule],
})
export class ClientsRoutingModule {
}
