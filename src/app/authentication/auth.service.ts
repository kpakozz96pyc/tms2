export class AuthService {
  isLoggedIn = false;

  logIn() {
    this.isLoggedIn = true;
  }

  isAuth() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.isLoggedIn);
      }, 1000);
    });
  }

  getGroups() {
    return {};
  }

  logOut() {
    this.isLoggedIn = false;
  }

}
